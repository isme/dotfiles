# Get the directory that contains the script. This is so that other scripts can
# be sourced even if .bashrc is souces from another directory.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# A minimal prompt, just like in the examples.
PS1=\$\ 

# Load a Homebrew API token so that brew's API calls don't fail.
source "${DIR}/.homebrew_token"

# Add command completion for AWS CLI.
complete -C "$(which aws_completer)" aws

# Add command completion for git.
source /usr/local/etc/bash_completion

# So that git diffs show UTF-8 correctly.
export LESSCHARSET="utf-8"

# Enable rbenv.
eval "$(rbenv init -)"

# Fix `kcn ssh`.
ssh-add -A
