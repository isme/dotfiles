# Mac OS X's terminal sources .profile when it starts.  Although bash is the
# default shell, it doesn't source .bashrc by default.
source ~/.bashrc
